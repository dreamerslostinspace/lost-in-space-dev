﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class IInventoryObject : MonoBehaviour
{
    public bool inInventory;
    public bool canBePlaced;

    // Start is called before the first frame update
    void Start()
    {
        canBePlaced = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (inInventory && gameObject.layer == LayerMask.NameToLayer("Item"))
            gameObject.layer = LayerMask.NameToLayer("ItemInInventory");

        if (!inInventory && gameObject.layer == LayerMask.NameToLayer("ItemInInventory"))
            gameObject.layer = LayerMask.NameToLayer("Item");
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Item") || collision.gameObject.layer == LayerMask.NameToLayer("ItemInInventory"))
        {
            canBePlaced = false;
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Item") || collision.gameObject.layer == LayerMask.NameToLayer("ItemInInventory"))
        {
            canBePlaced = true;
        }
    }
}
