﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory1 : MonoBehaviour
{
    public Grid invGrid;

    public int width;
    public int height;
    public float snapOffset;

    public List<GameObject> items;

    public GameObject highlightCellPrefab;

    // Start is called before the first frame update
    void Start()
    {
        invGrid = GetComponent<Grid>();
        GetComponent<BoxCollider>().size = new Vector3(width, height, 1f);

        for (int w = -width/2; w < width/2; w++)
            for(int h = -height/2; h < height/2; h++)
            {
                Vector3 pos = invGrid.GetCellCenterLocal(invGrid.LocalToCell(new Vector3((int)w * invGrid.cellSize.x, (int)h * invGrid.cellSize.y, 0)));
                Instantiate(highlightCellPrefab, pos, Quaternion.identity, transform);
            }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collision)
    {
        GameObject go = collision.gameObject;
        if (!items.Contains(go))
            items.Add(go);

        //go.GetComponent<MeshRenderer>().bounds.

    }

    void OnTriggerExit(Collider collision)
    {
        GameObject go = collision.gameObject;
        if (items.Contains(go))
            items.Remove(go);
    }

    public void SnapToGrid(GameObject item)
    {
        Vector3 gridPos = invGrid.WorldToCell(item.transform.position);
        float posx = item.GetComponent<MeshRenderer>().bounds.size.x % 2 > 1 ? gridPos.x : gridPos.x + invGrid.cellSize.x / 2;
        float posy = item.GetComponent<MeshRenderer>().bounds.size.y % 2 > 1 ? gridPos.y : gridPos.y + invGrid.cellSize.y / 2;

        item.transform.position = new Vector3(posx, posy);
    }

    public bool isItemInBounds(GameObject item)
    {
        Vector3 bMax = item.GetComponent<MeshRenderer>().bounds.max;
        Vector3 bMin = item.GetComponent<MeshRenderer>().bounds.min;


        return GetComponent<BoxCollider>().bounds.Contains(new Vector3(bMax.x - snapOffset, bMax.y - snapOffset, bMax.z - snapOffset)) 
            && GetComponent<BoxCollider>().bounds.Contains(bMin);
    }

}
