﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryDragNDropHandler : MonoBehaviour
{
    public float interactionRange;
    public Transform player;

    public Vector3 dragStartPosition;
    public GameObject draggedItem;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        if (Vector2.Distance(origin, player.position) <= interactionRange)
        {
            OnBeginDrag(origin);
            OnDrag(origin);
            OnEndDrag(origin);
        }
    }

    
    
    private void OnBeginDrag(Vector3 origin)
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))   //On Begin Drag
        {
            RaycastHit hitInfo;

            bool hit = Physics.Raycast(origin, Vector3.forward, out hitInfo, 100, LayerMask.GetMask("Item", "ItemInInventory", "Ship Exterior"));
            if (hit && hitInfo.collider.gameObject.layer != LayerMask.NameToLayer("Ship Exterior"))
            {
                draggedItem = hitInfo.collider.gameObject;
                dragStartPosition = draggedItem.transform.position;

                draggedItem.transform.rotation = Quaternion.identity;
                draggedItem.GetComponent<Rigidbody>().velocity = Vector3.zero;
                draggedItem.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                draggedItem.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            }
        }
    }

    private void OnDrag(Vector3 origin)
    {
        if (Input.GetKey(KeyCode.Mouse0))       //On Drag
        {
            if (isDragging())
            {
                draggedItem.transform.position = new Vector3(origin.x, origin.y, 0f);

                RaycastHit hitInfo;
                bool hit = Physics.Raycast(origin, Vector3.forward, out hitInfo, 100, LayerMask.GetMask("Inventory", "Ship Exterior"));
                if (hit 
                    && hitInfo.collider.gameObject.layer != LayerMask.NameToLayer("Ship Exterior"))
                {
                    if (hitInfo.collider.gameObject.GetComponent<Inventory1>().isItemInBounds(draggedItem))
                        hitInfo.collider.gameObject.GetComponent<Inventory1>().SnapToGrid(draggedItem);
                }
            }
        }
    }

    private void OnEndDrag(Vector3 origin)
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))     //On End Drag
        {
            if (isDragging())
            {
                IInventoryObject draggedItemInvObj = draggedItem.GetComponent<IInventoryObject>();

                RaycastHit hitInfo;
                bool hit = Physics.Raycast(origin, Vector3.forward, out hitInfo, 100, LayerMask.GetMask("Inventory", "Ship Exterior"));

                if (hit && hitInfo.collider.gameObject.layer == LayerMask.NameToLayer("Ship Exterior"))
                    draggedItem.transform.position = dragStartPosition;
                else 
                if (hit
                    && draggedItemInvObj.canBePlaced 
                    && hitInfo.collider.gameObject.layer == LayerMask.NameToLayer("Inventory")
                    && hitInfo.collider.gameObject.GetComponent<Inventory1>().isItemInBounds(draggedItem))
                {
                    draggedItem.transform.position = new Vector3(origin.x, origin.y, 0f);   //recallibration before snapping
                    hitInfo.collider.gameObject.GetComponent<Inventory1>().SnapToGrid(draggedItem);

                    draggedItemInvObj.inInventory = true;
                }
                else
                {
                    if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
                        if (draggedItemInvObj.inInventory && draggedItemInvObj.canBePlaced)
                            draggedItemInvObj.inInventory = false;

                    if (draggedItemInvObj.inInventory || !draggedItemInvObj.canBePlaced)
                        draggedItem.transform.position = dragStartPosition;

                    if (!draggedItemInvObj.inInventory)
                        draggedItem.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;

                }
                draggedItem = null;
            }
        }
    }

    public bool isDragging()
    {
        return draggedItem != null;
    }

}
