﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiS_Event : MonoBehaviour
{
    public int id;
    public string title;
    public bool fired;

    public virtual bool CheckingStartCondition()
    {
        return false;
    }

    public virtual void Activation() { }
    public virtual void AddNewQuest() { }
    public virtual void AddJournalEntry() { }
}
