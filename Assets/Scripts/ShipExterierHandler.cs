﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipExterierHandler : MonoBehaviour
{
    public float threshhold;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Camera.main.orthographicSize <= threshhold)
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            GetComponent<MeshRenderer>().enabled = true;
            GetComponent<BoxCollider>().enabled = true;
        }
    }
    
}
