﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest : MonoBehaviour
{
    public int iD;
    public string title;
    public string type;
    public int stage;
    public bool active = false;
    public bool completed = false;

    public virtual bool CheckingStartCondition()
    {
        if (completed == true || active == true)
        {
            return false;
        }

        return true;
    }

    public virtual bool CheckingEndCondition() { return false; }

    public virtual void Activation()
    {
        this.active = true;
    }

    public virtual void Completion()
    {
        this.active = false;
        this.completed = true;
    }

    public virtual void AddToQuestLog() { }
    public virtual void AddJournalEntry() { }
    public virtual void AwardReward() { }
}
