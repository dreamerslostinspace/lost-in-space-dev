﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour
{
    // unique object
    static SceneManager Instance;

    // all scenes
    string videoOpeningSceneName = "GameOpening";
    string mainMenuSceneName = "MainMenuScene";
    string gameSceneName = "GameScene";

    // current scene and scene name
    Scene currentScene;
    string thisSceneName;

    // load main menu
    public void LoadMainMenuScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(mainMenuSceneName, LoadSceneMode.Single);
    }

    // load game scene
    public void LoadGameScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(gameSceneName, LoadSceneMode.Single);
    }

    // quit game
    public void QuitGame()
    {
        Application.Quit();
    }

    void Start()
    {
        // if object already exists, destroy it and 
        // create new one
        if (Instance != null)
        {
            GameObject.Destroy(gameObject);
        }
        else
        {
            GameObject.DontDestroyOnLoad(gameObject);
            Instance = this;
        }

        // fetch name of current scene
        currentScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene(); // ... because I called the class "Scene Manager"
        thisSceneName = currentScene.name;
    }

    void Update()
    {
        // opening video scene
        // either redirect to menu at the end of the video
        // or redirect when user presses ESC
        if (thisSceneName == videoOpeningSceneName && Input.GetKeyDown(KeyCode.Escape))
        {
            LoadMainMenuScene();
        }

    }
}
