﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float zoomSpeed;
    public float maxZoomIn;
    public float maxZoomOut;
    public float zoom;


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        zoom = (Input.GetAxisRaw("Mouse ScrollWheel") * zoomSpeed);
        GetComponent<Camera>().orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize - zoom, maxZoomIn, maxZoomOut);
    }

    public float GetZoom()
    {
        return GetComponent<Camera>().orthographicSize;
    }
}