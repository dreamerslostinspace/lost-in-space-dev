﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class LoadSceneAfterVideoEnded : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    string nextSceneName = "MainMenuScene";

    // detect when video is stopping
    private void Start()
    {
        videoPlayer.loopPointReached += LoadScene;
    }

    // load the next scene which is the main menu
    void LoadScene(VideoPlayer vp)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(nextSceneName, LoadSceneMode.Single);
    }

}