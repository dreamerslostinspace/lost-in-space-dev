﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.Audio;
using TMPro;

public class OptionsMenuManager : MonoBehaviour
{
    // information from UI
    public TextMeshProUGUI[] resolutionTexts;
    public UnityEngine.UI.Toggle fullscreenToggle;
    public UnityEngine.UI.Slider mouseSensSlider;
    public UnityEngine.UI.Slider musicSlider;
    public UnityEngine.UI.Slider effectsSlider;

    public AudioMixer musicAudioMixer;
    public AudioMixer effectsAudioMixer;
    private Resolution[] resolutions;

    private void Start()
    {
        resolutions = Screen.resolutions;

        for(int i = 0; i < resolutions.Length; i++)
        {
            Debug.Log(resolutions[i]);
        }
        Debug.Log("current res: " + Screen.currentResolution);
    }

    // change mouse sensitivity
    public void ChangeMouseSensitivity(float value)
    {

    }

    // change volume of music - DIRECTLY
    public void ChangeMusicVolume(float volume)
    {
        musicAudioMixer.SetFloat("MusicVolume", volume);
    }

    // change volume of sound effects - DIRECTLY
    public void ChangeEffectsVolume(float volume)
    {
        musicAudioMixer.SetFloat("EffectsVolume", volume);
    }

    // set fullscreen
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
    }
}
