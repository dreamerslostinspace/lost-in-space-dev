﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCameraController : MonoBehaviour
{
    public Transform [] screens= new Transform[4];
    public float transitionSpeed;
    public Transform currentScreen;

    public void GoToMenu()
    {
        currentScreen = screens[0];
    }

    public void GoToLoad()
    {
        currentScreen = screens[1];
    }

    public void GoToOptions()
    {
        currentScreen = screens[2];
    }

    public void GoToCredits()
    {
        currentScreen = screens[3];
    }

    public Transform GetCurrentScreen()
    {
        return currentScreen;
    }

    public Transform[] GetAllScreens()
    {
        return screens;
    }


    private void Start()
    {
        currentScreen = screens[0];
    }   

    void LateUpdate()
    {
        //Lerp position
        transform.position = Vector3.Lerp(transform.position, currentScreen.position, Time.deltaTime * transitionSpeed);

        Vector3 currentAngle = new Vector3(
         Mathf.LerpAngle(transform.rotation.eulerAngles.x, currentScreen.transform.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
         Mathf.LerpAngle(transform.rotation.eulerAngles.y, currentScreen.transform.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
         Mathf.LerpAngle(transform.rotation.eulerAngles.z, currentScreen.transform.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed));

        transform.eulerAngles = currentAngle;
    }
}