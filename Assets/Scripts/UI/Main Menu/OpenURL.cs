﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenURL : MonoBehaviour
{
    public void OpenThisURL()
    {
        Application.OpenURL("https://dreamers-lostinspace.webnode.cz/");
    }
}
