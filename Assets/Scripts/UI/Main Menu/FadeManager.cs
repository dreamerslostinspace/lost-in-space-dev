﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    private Camera camera;
    private MainMenuCameraController cameraController;

    public GameObject inSceneCamera;

    [SerializeField] public float waitingDuration;
    [SerializeField] public float fadeDuration;

    [SerializeField] public CanvasGroup mainMenuTitles;
    [SerializeField] public CanvasGroup mainMenuButtons;

    [SerializeField] public CanvasGroup loadMenuBackButton;

    [SerializeField] public CanvasGroup optionsMenuBackButtons;

    [SerializeField] public CanvasGroup creditsMenuBackButtons;

    private Transform cameraTransform;
    private Transform[] screens;
    public Transform currentScreen;

    private void SetAplphaToZeroForAll()
    {
        mainMenuTitles.alpha = 0;
        mainMenuButtons.alpha = 0;
        loadMenuBackButton.alpha = 1; // debug
        optionsMenuBackButtons.alpha = 1; // debug
        creditsMenuBackButtons.alpha = 1; // debug
    }

    void ManageFading()
    {
        // main menu
        if(currentScreen == screens[0])
        {
            FadeOutLoadMenu();
            FadeOutOptionsMenu();
            FadeOutCreditsMenu();

            FadeInMainMenu();
        }

        // load menu
        if(currentScreen == screens[1])
        {
            FadeOutMainMenu();
            FadeOutOptionsMenu();
            FadeOutCreditsMenu();

            FadeInLoadMenu();
        }

        // options menu
        if(currentScreen == screens[2])
        {
            FadeOutMainMenu();
            FadeOutLoadMenu();
            FadeOutCreditsMenu();

            FadeInOptionsMenu();
        }

        // credits menu
        if(currentScreen == screens[3])
        {
            FadeOutMainMenu();
            FadeOutLoadMenu();
            FadeOutOptionsMenu();

            FadeInCreditsMenu();
        }
    }

    void FadeInMainMenu()
    {
        StartCoroutine(FadeCanvas(fadeDuration, mainMenuTitles));
        StartCoroutine(FadeCanvas(fadeDuration + 2.0f, mainMenuButtons));
    }

    void FadeOutMainMenu()
    {

    }

    void FadeInLoadMenu()
    {

    }

    void FadeOutLoadMenu()
    {

    }

    void FadeInOptionsMenu()
    {

    }

    void FadeOutOptionsMenu()
    {

    }

    void FadeInCreditsMenu()
    {

    }

    void FadeOutCreditsMenu()
    {

    }

    // manage the fading
    private IEnumerator FadeCanvas(float t, CanvasGroup cg)
    {
        float startTime = Time.time;
        float endTime = startTime + fadeDuration;
        float elapsedTime = 0f;
        float percentage;
        float initialAlpha = cg.alpha;

        while (Time.time < endTime)
        {
            elapsedTime = Time.time - startTime;
            percentage = 1 / (fadeDuration / elapsedTime);

            if (initialAlpha == 1)
            {
                Debug.Log("One");
                cg.alpha = initialAlpha - percentage;
            }
            else if(initialAlpha == 0)
            {
                Debug.Log("Zero");
                cg.alpha = initialAlpha + percentage;
            }

            yield return null;
        }
    }


    void Start()
    {
        // set up camera and camera controller script
        camera = inSceneCamera.GetComponent<Camera>();
        cameraController = camera.GetComponent<MainMenuCameraController>();

        // set all ui visibility to 0
        SetAplphaToZeroForAll();

        // get all screens for comparison
        screens = cameraController.GetAllScreens();
    }



    // Update is called once per frame
    void Update()
    {
        // get current screen and fade in/out accordingly
        currentScreen = cameraController.GetCurrentScreen();
        ManageFading();

        if (Input.GetKey(KeyCode.F))
        {
            StartCoroutine(FadeCanvas(fadeDuration, mainMenuTitles));
            StartCoroutine(FadeCanvas(fadeDuration + 2.0f, mainMenuButtons));
        }
    }
}
