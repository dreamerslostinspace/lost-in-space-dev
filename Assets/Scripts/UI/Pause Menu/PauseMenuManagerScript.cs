﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuManagerScript : MonoBehaviour
{
    [SerializeField] public GameObject pauseMenuUI;
    [SerializeField] public GameObject mainMenuDialogue;
    [SerializeField] public GameObject quitDialogue;

    private bool pauseMenuActive;
    private bool mainMenuDialogueActive;
    private bool quitDialogueActive;

    private void HandleVisibility()
    {
        if (pauseMenuActive)
        {
            ActivatePauseMenu();
        }
        else
        {
            DeactivatePauseMenu();
        }
    }

    // declared public for "Resume Button Script"
    public void DeactivatePauseMenu()
    {
        // un-freeze game
        Time.timeScale = 1f;

        // deactivate pause menu
        pauseMenuUI.SetActive(false);
    }

    private void ActivatePauseMenu()
    {
        // freeze game
        Time.timeScale = 0f;

        // activate pause menu
        pauseMenuUI.SetActive(true);
    }

    // deactivating main menu dialogue
    public void DeactivateMainMenuDialogue()
    {
        mainMenuDialogueActive = false;
        mainMenuDialogue.SetActive(false);
    }

    // activating main menu dialogue
    public void ActivateMainMenuDialogue()
    {
        mainMenuDialogueActive = true;
        mainMenuDialogue.SetActive(true);
    }

    // deactivating quit dialogue
    public void DeactivateQuitDialogue()
    {
        quitDialogueActive = false;
        quitDialogue.SetActive(false);
    }

    // activating quit dialogue
    public void ActivateQuitDialogue()
    {
        quitDialogueActive = true;
        quitDialogue.SetActive(true);
    }

    // quit the game
    public void QuitGame()
    {
        Application.Quit();
    }

    private void Start()
    {
        // disable all ui panels
        pauseMenuUI.SetActive(false);
        mainMenuDialogue.SetActive(false);
        quitDialogue.SetActive(false);

        // set bools to false
        pauseMenuActive = false;
        mainMenuDialogueActive = false;
        quitDialogueActive = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (mainMenuDialogueActive)
            {
                DeactivateMainMenuDialogue();
            }
            else if (quitDialogueActive)
            {
                DeactivateQuitDialogue();
            }
            else
            {
                pauseMenuActive = !pauseMenuActive;
                HandleVisibility();
            }

        }
    }

}