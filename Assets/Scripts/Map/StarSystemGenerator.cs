﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Logic: 1) We determine how many planets we need and what type, 
//        2) Use a common planet pattern, generate a random place of the planet appearance (or use a fixed one when a certain type is required), 
//        3) Assign a planet type to the planet based on the distance from the sun, 
//        4) Choose the desired planet pattern 
//        5) Based on the type obtained we generate resources on it, 
//        6) We inform it of the location 
//        7) And place it in space.

public class StarSystemGenerator : MonoBehaviour
{
    [Header("Main Generation Settings")]
    public bool usingInspector;
    public bool usingSeed;

    [Header("Seed")]
    public string seed;

    [Header("Sun generation settings - type")]
    public bool randomSun;
    public bool red;
    public bool blue;
    public bool yellow;

    [Header("Sun generation settings - count")]
    public bool randomSunCount;
    public bool one;
    public bool two;
    public bool three;
    public int sunCount;

    [Header("Prefabs of the suns")]
    public GameObject normalSun;

    [Header("Planet generation settings")]
    public int minNumberOfPlanets;
    public int maxNumberOfPlanets;
    [Range(0.0f, 100.0f)]
    public int addMoltenPlanets;
    [Range(0.0f, 100.0f)]
    public int addEarthLikePlanets;
    [Range(0.0f, 100.0f)]
    public int addTombPlanets;
    [Range(0.0f, 100.0f)]
    public int addGasGiantPlanets;
    [Range(0.0f, 100.0f)]
    public int addFrozenPlanets;

    [Header("Asteroid generation settings")]
    public bool randomFields;
    public bool internalField;
    public bool middleField;
    public bool externalField;

    [Header("Prefab of the asteroid")]
    public GameObject asteroid;

    [Header("Resource generation settings")]
    public bool addHelium;
    public bool addIron;
    public bool addSilicium;
    public bool addGold;
    public bool addUranium;
    public bool addNuclearuel;
    public bool addWater;

    [Header("Prefabs of the planets")]
    public Planet defaultPlanet;
    public Planet moltenPlanet;
    public Planet twinEarthPlanet;
    public Planet gasGiantPlanet;
    public Planet frozenPlanet;
    public Planet tombPlanet;

    // PRIVATE
    //private Vector3 randomStartPosition;
    private int requestedPlanets;
    private int randomNumberOfPlanets;

    private GameObject questsManagerObject;
    private QuestsManager questsManager;
    private int questsTotalNumber;

    private GameObject systemCenter;
    private int requestedSuns;

    // Use this for initialization
    void Start()
    {
        requestedPlanets = 0;
        requestedSuns = 0;

        // Quest Manager
        questsManagerObject = GameObject.Find("QuestsManager");
        questsManager = questsManagerObject.GetComponent<QuestsManager>();
        questsTotalNumber = questsManager.GetTotalNumberOfQuests();

        // System Center Search
        systemCenter = GameObject.FindGameObjectWithTag("SystemsCenter");

        // We sum up and remember the number of requested planets.
        if (addEarthLikePlanets > 0)
        {
            requestedPlanets += addEarthLikePlanets;
        }
        if (addTombPlanets > 0)
        {
            requestedPlanets += addTombPlanets;
        }
        if (addMoltenPlanets > 0)
        {
            requestedPlanets += addMoltenPlanets;
        }
        if (addGasGiantPlanets > 0)
        {
            requestedPlanets += addGasGiantPlanets;
        }
        if (addFrozenPlanets > 0)
        {
            requestedPlanets += addFrozenPlanets;
        }

        // We determine the number of planets that we will generate. This is a random variable between the minimum and maximum values. 
        // At the same time, we can not generate fewer planets than was requested.
        if (requestedPlanets > minNumberOfPlanets)
        {
            randomNumberOfPlanets = Random.Range(requestedPlanets, maxNumberOfPlanets);
        }
        else
        {
            randomNumberOfPlanets = Random.Range(minNumberOfPlanets, maxNumberOfPlanets);
        }

        // Suns create
        if (one)
        {
            requestedSuns = 1;
            two = false;
            three = false;
            randomSunCount = false;
        }
        else if (two)
        {
            requestedSuns = 2;
            one = false;
            three = false;
            randomSunCount = false;
        }
        else if (three)
        {
            requestedSuns = 3;
            one = false;
            two = false;
            randomSunCount = false;
        }
        else if (randomSunCount)
        {
            requestedSuns = Random.Range(1,4);
            one = false;
            two = false;
            three = false;
        }

        CreateSun();

        sunCount = requestedSuns;

        // Create a certain number of planets.
        for (int i = 0; i < randomNumberOfPlanets; i++)
        {
            Planet newPlanet = new Planet(); // Create an instance of the class "Planet" as a template.
            CreatePlanet(newPlanet);         // We apply to it a method that determines the final appearance and location of the planet.
        }

        // Asteroids Fields create
        if (internalField || middleField || externalField)
        {
            CreateAsteroidsFields();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    // A method that establishes a resource with a specified ID as a frequently encountered resource for the transmitted planet pattern.
    public void SetFrequentResourceType(int resourceTypeIndex, Planet targetPlanet)
    {
        switch (resourceTypeIndex)
        {
            case 1:
                targetPlanet.frequentResource = Planet.resourceTypes.Helium;
                break;
            case 2:
                targetPlanet.frequentResource = Planet.resourceTypes.Iron;
                break;
            case 3:
                targetPlanet.frequentResource = Planet.resourceTypes.Silicium;
                break;
            case 4:
                targetPlanet.frequentResource = Planet.resourceTypes.Gold;
                break;
            case 5:
                targetPlanet.frequentResource = Planet.resourceTypes.Uranium;
                break;
            case 6:
                targetPlanet.frequentResource = Planet.resourceTypes.Nuclear_Fuel;
                break;
            case 7:
                targetPlanet.frequentResource = Planet.resourceTypes.Water;
                break;
        }
    }

    // A method that establishes a resource with a specified ID as a normally encountered resource for the transmitted planet pattern.
    public void SetNormalResourceType(int resourceTypeIndex, Planet targetPlanet)
    {
        switch (resourceTypeIndex)
        {
            case 1:
                targetPlanet.normalResource = Planet.resourceTypes.Helium;
                break;
            case 2:
                targetPlanet.normalResource = Planet.resourceTypes.Iron;
                break;
            case 3:
                targetPlanet.normalResource = Planet.resourceTypes.Silicium;
                break;
            case 4:
                targetPlanet.normalResource = Planet.resourceTypes.Gold;
                break;
            case 5:
                targetPlanet.normalResource = Planet.resourceTypes.Uranium;
                break;
            case 6:
                targetPlanet.normalResource = Planet.resourceTypes.Nuclear_Fuel;
                break;
            case 7:
                targetPlanet.normalResource = Planet.resourceTypes.Water;
                break;
        }
    }

    // A method that establishes a resource with a specified ID as a rarely encountered resource for the transmitted planet pattern.
    public void SetRareResourceType(int resourceTypeIndex, Planet targetPlanet)
    {
        switch (resourceTypeIndex)
        {
            case 1:
                targetPlanet.rareResource = Planet.resourceTypes.Helium;
                break;
            case 2:
                targetPlanet.rareResource = Planet.resourceTypes.Iron;
                break;
            case 3:
                targetPlanet.rareResource = Planet.resourceTypes.Silicium;
                break;
            case 4:
                targetPlanet.rareResource = Planet.resourceTypes.Gold;
                break;
            case 5:
                targetPlanet.rareResource = Planet.resourceTypes.Uranium;
                break;
            case 6:
                targetPlanet.rareResource = Planet.resourceTypes.Nuclear_Fuel;
                break;
            case 7:
                targetPlanet.rareResource = Planet.resourceTypes.Water;
                break;
        }
    }

    // The method that determines the type of planet, based on its distance from the star.
    public void SetPlanetType(float rangeFromSun, Planet targetPlanet)
    {
        // We control the distance to the star, on the basis of this we determine the type of planet. 
        // If there are requirements for planets of different types in one zone, we satisfy them in turn. 
        // When assigning a planet type, we always reduce the number of requested planets of this type.
        if (rangeFromSun < 300)
        {
            targetPlanet.type = Planet.planetTypes.Molten;
            addMoltenPlanets--;
        }
        else if (rangeFromSun >= 300 && rangeFromSun < 500)
        {
            if (addTombPlanets > 0)
            {
                targetPlanet.type = Planet.planetTypes.Tomb;
                addTombPlanets--;
            }
            else
            {
                targetPlanet.type = Planet.planetTypes.Twin_Earth;
                addEarthLikePlanets--;
            }
        }
        else if (rangeFromSun >= 500 && rangeFromSun < 700)
        {
            targetPlanet.type = Planet.planetTypes.Gas_Giant;
            addGasGiantPlanets--;
        }
        else if (rangeFromSun >= 700)
        {
            targetPlanet.type = Planet.planetTypes.Frozen;
            addFrozenPlanets--;
        }
    }

    // A method that randomly assigns resources to a selected planet based on its type.
    public void SetResourcesOfPlanet(Planet targetPlanet, Planet.planetTypes planetType)
    {
        // Each resource has its own ID, on each planet there are three cells for resources.
        int randomFrequentResourceIndex;
        int randomNormalResourceIndex;
        int randomRareResourceIndex;

        // Based on the type of planet...
        switch (planetType)
        {
            // For each cell, we define its resource, randomly generating a digit representing the ID of the resource type, 
            //    using as constraints the limits imposed by the planet type. Then we use a special method to assign it to the pattern of the planet.
            case Planet.planetTypes.Molten:

                if (addIron)
                {
                    randomFrequentResourceIndex = 2;
                    SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);
                    addIron = false;
                }
                else
                {
                    randomFrequentResourceIndex = Random.Range(2, 4);
                    SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);
                }

                randomNormalResourceIndex = Random.Range(2, 5);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(4, 6);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Twin_Earth:

                randomFrequentResourceIndex = Random.Range(7, 8);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(2, 6);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(4, 6);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Gas_Giant:

                randomFrequentResourceIndex = Random.Range(1, 2);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 2);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(1, 2);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Frozen:

                randomFrequentResourceIndex = Random.Range(7, 8);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 3);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(3, 6);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            case Planet.planetTypes.Tomb:

                randomFrequentResourceIndex = Random.Range(5, 6);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 7);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(6, 7);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;

            default:

                randomFrequentResourceIndex = Random.Range(1, 7);
                SetFrequentResourceType(randomFrequentResourceIndex, targetPlanet);

                randomNormalResourceIndex = Random.Range(1, 7);
                SetNormalResourceType(randomNormalResourceIndex, targetPlanet);

                randomRareResourceIndex = Random.Range(1, 7);
                SetRareResourceType(randomRareResourceIndex, targetPlanet);

                break;
        }
    }

    public void PlaceSun(Vector3 sunStartPosition, float scale)
    {
        normalSun.transform.localScale = new Vector3(scale,scale,scale);
        Instantiate(normalSun, sunStartPosition, Quaternion.identity);
    }

    // The method of assigning resources to the planet and placing it in space.
    public void PlacePlanet(Planet planetPrefab, Vector3 randomStartPosition)
    {
        // When choosing an action, we are based on the transmitted type of planet.
        switch (planetPrefab.type)
        {
            // Then we set the type of resources for a given planet, using not a general template, but a template of a specific type of planet.
            // Just in case, we additionally inform her of her type.
            // Directly place an instance of the planet pattern of the required type, in the place that was previously defined.
            case Planet.planetTypes.Molten:
                SetResourcesOfPlanet(moltenPlanet, Planet.planetTypes.Molten);
                moltenPlanet.type = Planet.planetTypes.Molten;
                float moltenPlanetSpeed = Random.Range(0.8f, 1.5f);
                float moltenPlanetSize = Random.Range(5.0f, 8.0f);
                SetPlanetSpeed(moltenPlanet, moltenPlanetSpeed);
                SetPlanetSize(moltenPlanet, moltenPlanetSize);
                QuestAssigments(moltenPlanet);
                moltenPlanet.startPosition = randomStartPosition;
                moltenPlanet.transform.position = randomStartPosition;
                Instantiate(moltenPlanet);
                break;
            case Planet.planetTypes.Twin_Earth:
                SetResourcesOfPlanet(twinEarthPlanet, Planet.planetTypes.Twin_Earth);
                twinEarthPlanet.type = Planet.planetTypes.Twin_Earth;
                float twinEarthPlanetSpeed = Random.Range(0.2f, 0.6f);
                float twinEarthPlanetSize = Random.Range(8.0f, 11.0f);
                SetPlanetSpeed(twinEarthPlanet, twinEarthPlanetSpeed);
                SetPlanetSize(twinEarthPlanet, twinEarthPlanetSize);
                QuestAssigments(twinEarthPlanet);
                twinEarthPlanet.startPosition = randomStartPosition;
                twinEarthPlanet.transform.position = randomStartPosition;
                Instantiate(twinEarthPlanet);
                break;
            case Planet.planetTypes.Gas_Giant:
                SetResourcesOfPlanet(gasGiantPlanet, Planet.planetTypes.Gas_Giant);
                gasGiantPlanet.type = Planet.planetTypes.Gas_Giant;
                float gasGiantPlanetSpeed = Random.Range(0.1f, 0.25f);
                float gasGiantSize = Random.Range(16.0f, 51.0f);
                SetPlanetSpeed(gasGiantPlanet, gasGiantPlanetSpeed);
                SetPlanetSize(gasGiantPlanet, gasGiantSize);
                QuestAssigments(gasGiantPlanet);
                gasGiantPlanet.startPosition = randomStartPosition;
                gasGiantPlanet.transform.position = randomStartPosition;
                Instantiate(gasGiantPlanet);
                break;
            case Planet.planetTypes.Frozen:
                SetResourcesOfPlanet(frozenPlanet, Planet.planetTypes.Frozen);
                frozenPlanet.type = Planet.planetTypes.Frozen;
                float frozenPlanetSpeed = Random.Range(0.04f, 0.1f);
                float frozenPlanetSize = Random.Range(4.0f, 11.0f);
                SetPlanetSpeed(frozenPlanet, frozenPlanetSpeed);
                SetPlanetSize(frozenPlanet, frozenPlanetSize);
                QuestAssigments(frozenPlanet);
                frozenPlanet.startPosition = randomStartPosition;
                frozenPlanet.transform.position = randomStartPosition;
                Instantiate(frozenPlanet);
                break;
            case Planet.planetTypes.Tomb:
                SetResourcesOfPlanet(tombPlanet, Planet.planetTypes.Tomb);
                tombPlanet.type = Planet.planetTypes.Tomb;
                float tombPlanetSpeed = Random.Range(0.2f, 0.4f);
                float tombPlanetSize = Random.Range(7.0f, 11.0f);
                SetPlanetSpeed(tombPlanet, tombPlanetSpeed);
                SetPlanetSize(tombPlanet, tombPlanetSize);
                QuestAssigments(tombPlanet);
                tombPlanet.startPosition = randomStartPosition;
                tombPlanet.transform.position = randomStartPosition;
                Instantiate(tombPlanet);
                break;
        }
    }

    public void PlaceAsteroid(Vector3 coordinates)
    {
        float asteroidSize = Random.Range(0.5f, 2.0f);
        SetAsteroidSize(asteroid, asteroidSize);
        Instantiate(asteroid, coordinates, Quaternion.identity);
    }

    public void SetPlanetSpeed(Planet targetPlanet, float newSpeed)
    {
        targetPlanet.speed = newSpeed;
    }

    public void SetPlanetSize(Planet targetPlanet, float newSize)
    {
        targetPlanet.size = newSize;
    }

    public void SetAsteroidSize (GameObject asteroid, float newSize)
    {
        asteroid.transform.localScale = new Vector3(newSize, newSize, newSize);
    }

    public void QuestAssigments(Planet targetPlanet)
    {
        bool check = false;
        int randomQuestID = Random.Range(0, questsTotalNumber);
        check = questsManager.CheckStartCondition(randomQuestID);

        if (check)
        {
            Debug.Log("NEW QUEST: " + "ID: " + randomQuestID + " " + "Time: " + System.DateTime.Now);
            questsManager.QuestActivation(randomQuestID);
            targetPlanet.questIsHere = true;
            targetPlanet.questID = randomQuestID;
        }
        else
        {
            targetPlanet.questIsHere = false;
            targetPlanet.questID = -1;
        }
    }

    public void CreateSun()
    {
        if (requestedSuns == 1)
        {
            Vector3 sunStartPosition = new Vector3(0, 0, 0);
            float scale = 1.5f;
            PlaceSun(sunStartPosition,scale);
        }
        else if (requestedSuns == 2)
        {
            Vector3 sunStartPosition1 = new Vector3(-30, -30, 0);
            float scale1 = 0.6f;
            PlaceSun(sunStartPosition1, scale1);

            Vector3 sunStartPosition2 = new Vector3(30, 30, 0);
            float scale2 = 0.3f;
            PlaceSun(sunStartPosition2, scale2);
        }
        else if (requestedSuns == 3)
        {
            Vector3 sunStartPosition1 = new Vector3(-30, -30, 0);
            float scale1 = 0.4f;
            PlaceSun(sunStartPosition1, scale1);

            Vector3 sunStartPosition2 = new Vector3(30, -10, 0);
            float scale2 = 0.3f;
            PlaceSun(sunStartPosition2, scale2);

            Vector3 sunStartPosition3 = new Vector3(0, 35, 0);
            float scale3 = 0.2f;
            PlaceSun(sunStartPosition3, scale3);
        }
    }

    // Main method for creating a planet. Randomly generates a starting position for it and calculates the distance to the sun. 
    // Then it calls methods that establish the type of planet and place it in space.
    public void CreatePlanet(Planet planet)
    {
        Vector3 randomStartPosition = new Vector3();

        // If there are requirements to create a planet of a certain type, 
        //    then as coordinates we use the coordinates of the zone in which it may appear. Otherwise, we determine the coordinates randomly.
        if (addEarthLikePlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(300.0f, 350.0f), Random.Range(300.0f, 350.0f), 0);
        }
        else if (addMoltenPlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(80.0f, 85.0f), Random.Range(80.0f, 85.0f), 0);
        }
        else if (addTombPlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(300.0f, 350.0f), Random.Range(300.0f, 350.0f), 0);
        }
        else if (addGasGiantPlanets > 0)
        {
            randomStartPosition = new Vector3(Random.Range(450.0f, 520.0f), Random.Range(450.0f, 520.0f), 0);
        }
        else if (addFrozenPlanets > 0)
        {
            randomStartPosition = new Vector3((Random.Range(900.0f, 1000.0f)), (Random.Range(900.0f, 1000.0f)), 0);
        }
        else
        {
            randomStartPosition = new Vector3(Random.Range(-1000.0f, 1000.0f), Random.Range(-1000.0f, 1000.0f), 0);
        }

        // Calculate the distance from the planet to the sun.
        float rangeFromSun = Vector2.Distance(new Vector2(randomStartPosition.x, randomStartPosition.y), new Vector2(0, 0));

        // Call the method that determines the type of planet.
        SetPlanetType(rangeFromSun, planet);

        // Call the method that places the planet in space.
        PlacePlanet(planet, randomStartPosition);
    }

    public void CreateAsteroidsFields()
    {
        if (internalField)
        {
            for (int i = 0; i <= 50; i++)
            {
                Vector3 randomAsteroidStartPosition = new Vector3(Random.Range(100.0f, 120.0f), Random.Range(100.0f, 120.0f), 0);
                PlaceAsteroid(randomAsteroidStartPosition);
            }
        }
        if (middleField)
        {
            for (int i = 0; i <= 150; i++)
            {
                Vector3 randomAsteroidStartPosition = new Vector3(Random.Range(200.0f, 220.0f), Random.Range(200.0f, 220.0f), 0);
                PlaceAsteroid(randomAsteroidStartPosition);
            }
        }
        if (externalField)
        {
            for (int i = 0; i <= 200; i++)
            {
                Vector3 randomAsteroidStartPosition = new Vector3(Random.Range(350.0f, 400.0f), Random.Range(350.0f, 400.0f), 0);
                PlaceAsteroid(randomAsteroidStartPosition);
            }
        }
    }
}
