﻿using UnityEngine;

public class Comet : SpaceObject
{
    [Header("Comet")]
    public Vector3 direction;
    public double timeOfCreation;

    public bool flowEnd;


    void Start()
    {
        material = new Item("Water", Random.Range(1, 6));
        speed = Random.Range(0.2f, 0.5f);

        rb.MoveRotation( Quaternion.LookRotation(Environment.GetDirection(gameObject, player), Vector3.forward));

        Move();
    }

    /*public void HitTheShip()
    {
        if (Input.GetKeyUp("f"))
        {
            if (Environment.GetInteraction(player, comet, distance, comet.transform.localScale.x) && !flowEnd)
            {
                if (GameObject.Find("Inventory").GetComponent<Inventory>().AddItem(material.Name, material.Amount))
                {
                    AudioClip pickupSound = Resources.Load<AudioClip>("Sounds/comet pickup");
                    comet.GetComponent<AudioSource>().PlayOneShot(pickupSound);

                    comet.GetComponent<Renderer>().enabled = false;
                    flowEnd = true;
                }
               
            }
        }
    }*/
}
