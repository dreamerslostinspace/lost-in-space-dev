﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CometHandler : MonoBehaviour
{
    public GameObject cometPrefab;
    public GameObject comet;
    public GameObject player;

    public float disapearRange;
    
    // Use this for initialization
    void Start()
    {
        comet = Instantiate(cometPrefab, Environment.GetRandomPositionOffScreen(), Quaternion.identity, transform);
    }

    void Update()
    {
        if (comet == null)
            comet = Instantiate(cometPrefab, Environment.GetRandomPositionOffScreen(), Quaternion.identity, transform);

        if (Vector3.Distance(comet.transform.position, player.transform.position) > disapearRange)
        {
            Destroy(comet);
            comet = null;
        }
    }
}
