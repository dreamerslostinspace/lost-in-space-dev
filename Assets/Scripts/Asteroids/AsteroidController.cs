﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    private float powerForce = 2.0F; // Impact force on an object in Unity units

    public int asteroidCount;

    [Header("Spawn Chances")]
    public float chanceOfKillerAsteroid;
    public float chanceOfSpecialAsteroid;

    [Header("Prefabs")]
    public GameObject KillingAsteroid;
    public GameObject UraniumAsteroid;
    public GameObject ShipPartAsteroid;
    public GameObject MineAsteroid;

    void Update()
    {
        if (transform.childCount < asteroidCount)
        {
            CreateAsteroid();
        }
    }

    //Create new asteroid. If probability of killing == 1, then all asteroids are killing.
    private void CreateAsteroid(bool isKiller = false)
    {
        if (isKiller)
        {
            Instantiate(KillingAsteroid, Environment.GetRandomPositionOffScreen(), Quaternion.identity, transform);
        }
        else
        {
            float rand = (Random.Range(0f, 1f));

            if (rand < chanceOfKillerAsteroid)
            {
                Instantiate(KillingAsteroid, Environment.GetRandomPositionOffScreen(), Quaternion.identity, transform);
            }
            if (rand < chanceOfKillerAsteroid + chanceOfSpecialAsteroid)
            {
                if (Random.Range(0f, 1f) <= 0.5f) //uranium 5
                {
                    Instantiate(UraniumAsteroid, Environment.GetRandomPositionOffScreen(), Quaternion.identity, transform);
                }
                else                         //shippart 6 
                {
                    Instantiate(ShipPartAsteroid, Environment.GetRandomPositionOffScreen(), Quaternion.identity, transform);
                }
            }
            else    //basic asteroid
            {
                Instantiate(MineAsteroid, Environment.GetRandomPositionOffScreen(), Quaternion.identity, transform);
            }
        }
    }

    private void OnTriggerEnter(Collider collision) // Collision handling
    {
        if (collision.tag == "SafeZone") // If an asteroid collides with a safety zone
        {
            /* Calculate the rebound path */
            Vector3 direction = (this.transform.position - collision.transform.position);
            /* We act on the object giving impulse of the established force (multiplied by the direction) */
            gameObject.GetComponent<Rigidbody>().AddForce(direction * powerForce, ForceMode.Impulse);
        }

        if (collision.tag == "Player") // If an asteroid collides with a player
        {
            Vector3 direction = (this.transform.position - collision.transform.position);
            gameObject.GetComponent<Rigidbody>().AddForce(direction * powerForce, ForceMode.Impulse);
        }
    }
}
