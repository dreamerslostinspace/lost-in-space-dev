﻿using UnityEngine;

public class SpaceObject : MonoBehaviour
{
    [Header("SpaceObject")]
    public GameObject player;
    public float speed;
    public Item material;

    protected Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.Find("SpaceShip");
    }

    // Start is called before the first frame update
    void Start()
    {
        Move();
    }

    // Update is called once per frame
    void Update()
    {

    }

    protected void Move()
    {
        Vector3 direction = Environment.GetDirection(gameObject, player);
        rb.AddForce(direction * speed, ForceMode.VelocityChange);
    }

   

}
