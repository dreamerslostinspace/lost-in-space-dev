﻿using UnityEngine;

public static class Environment
{
    public static bool CheckEndOfView(GameObject player, GameObject asteroidy, double timeOfCreation, int elongation) //Check, if object is behind the player view or not.
    {
        if (player.GetComponent<GameController>().playtime > timeOfCreation + elongation)
        {
            GameObject camera = GameObject.Find("Main Camera");
            double distance = Mathf.Sqrt(Mathf.Pow((float)(camera.GetComponent<Camera>().orthographicSize * 2.4), 2) + Mathf.Pow(camera.GetComponent<Camera>().orthographicSize, 2));
            if (!GetInteraction(player, asteroidy, distance))
            {
                return true;
            }
        }
        return false;
    }

    public static bool GetInteraction(GameObject g1, GameObject g2, double distance, double size = 0) //Return true, if two objects are in given distance (+ size of object).
    {
        float dist = Vector3.Distance(g1.transform.position, g2.transform.position);

        return dist <= distance + size;
    }

    /// <summary>
    /// Generating random positions behind all borders of player view. 
    /// </summary>
    public static Vector3 GetRandomPositionOffScreen(float minDistance = 0, float maxDistance = 1)
    {
        if (Camera.main == null)
            return Vector3.zero;

        float posX = Random.Range(-1.00f, 1.00f);
        float posY = Random.Range(-1.00f, 1.00f);

        posX = posX >= 0 ? posX * maxDistance + 1 + minDistance : posX * maxDistance - minDistance;
        posY = posY >= 0 ? posY * maxDistance + 1 + minDistance: posY * maxDistance - minDistance;

        Vector3 point = Camera.main.ViewportToWorldPoint(new Vector2(posX, posY));
        point = new Vector3(point.x, point.y, 0f);
        return point;
    }

    //returns direction 
    public static Vector3 GetDirection(GameObject from, GameObject to)
    {
        Vector3 dir = to.transform.position - from.transform.position;
        return dir.normalized;
    }

    //returns if point is on screen (in main camera view)
    public static bool OnScreen(Vector3 position)
    {
        if (Camera.main == null)
            return false;

        Vector3 camBotLeftPos = Camera.main.ViewportToWorldPoint(Vector2.zero);
        Vector3 camTopRightPos = Camera.main.ViewportToWorldPoint(Vector2.one);

        Rect screen = new Rect(camBotLeftPos, camTopRightPos - camBotLeftPos);

        return screen.Contains(position);
    }
}
